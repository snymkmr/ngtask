import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { ModalComponent } from './_directives';
import { ModalService } from './_services';
import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { ProductListComponent } from './product-list/product-list.component';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot([{ path: '', component: ProductListComponent }])
  ],
  declarations: [AppComponent, ModalComponent, TopBarComponent, ProductListComponent],
  bootstrap: [AppComponent],
  providers: [ModalService]
})
export class AppModule {}
