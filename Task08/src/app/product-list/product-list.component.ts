import { Component, OnInit } from '@angular/core';

import { products } from '../products';
import { ModalService } from '../_services';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  private bodyText: string;
  products = products;

  constructor(private modalService: ModalService) {
  }
  
  ngOnInit() {
    this.bodyText = 'This text can be updated in modal 1';
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }
  share() {
    window.alert('The product has been shared!');
  }
}
