import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UpprcsComponent } from './upprcs/upprcs.component';
import { LwrcsComponent } from './lwrcs/lwrcs.component';

@NgModule({
  declarations: [
    AppComponent,
    UpprcsComponent,
    LwrcsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
