import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LwrcsComponent } from './lwrcs.component';

describe('LwrcsComponent', () => {
  let component: LwrcsComponent;
  let fixture: ComponentFixture<LwrcsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LwrcsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LwrcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
