import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lwrcs',
  templateUrl: './lwrcs.component.html',
  styleUrls: ['./lwrcs.component.css']
})
export class LwrcsComponent {
  message: string;
  typ = {
    case: 'LowerCase'
  };
  setMsg(data: string) {
    this.message = data;
  }
}
