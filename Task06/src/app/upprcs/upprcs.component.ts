import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-upprcs',
  templateUrl: './upprcs.component.html',
  styleUrls: ['./upprcs.component.css']
})
export class UpprcsComponent {
  inputData: string;
  typ = {
    case: 'UpperCase'
  };
  setData(data: string) {
    this.inputData = data;
  }
}
