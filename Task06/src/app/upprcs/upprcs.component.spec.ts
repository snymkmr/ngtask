import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpprcsComponent } from './upprcs.component';

describe('UpprcsComponent', () => {
  let component: UpprcsComponent;
  let fixture: ComponentFixture<UpprcsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpprcsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpprcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
