import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Task05';
  animals = ['Dog', 'Cat', 'Lion', 'Tiger'];
  defaultAnimal = 'Dog';
}
