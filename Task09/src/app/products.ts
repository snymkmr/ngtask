export const products = [
    {
      name: 'Model-S',
      price: 799,
      description: 'Tesla\'s impact on America\'s automotive landscape was miniscule until the game-changing Model S sedan came along.'
    },
    {
      name: 'Model-X',
      price: 699,
      description: 'The 2019 Tesla Model X might be the greenest—and one of the fastest—way to tote up to seven people over hill and dale.'
    },
    {
      name: 'Model-3',
      price: 299,
      description: 'It has a similar driving range and semi-autonomous driving tech as the Model S, but for half the price.'
    }
  ];
