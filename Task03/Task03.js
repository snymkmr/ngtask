//Write a simple program to display the day of the week based on the date/ (1 - 7) entered. Pls try to use Enum and Generics to implement the same
var readline = require('readline');
var Days;
(function (Days) {
    Days[Days["Sunday"] = 1] = "Sunday";
    Days[Days["Monday"] = 2] = "Monday";
    Days[Days["Tuesday"] = 3] = "Tuesday";
    Days[Days["Wednesday"] = 4] = "Wednesday";
    Days[Days["Thursday"] = 5] = "Thursday";
    Days[Days["Friday"] = 6] = "Friday";
    Days[Days["Saturday"] = 7] = "Saturday";
})(Days || (Days = {}));
function getDay(d) {
    console.log(Days[d]);
    return Days[d];
}
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
rl.question('Enter the sequence number for week days [1-7] --> ', function (answer) {
    switch (answer) {
        case "1":
            getDay(1);
            break;
        case "2":
            getDay(2);
            break;
        case "3":
            getDay(3);
            break;
        case "4":
            getDay(4);
            break;
        case "5":
            getDay(5);
            break;
        case "6":
            getDay(6);
            break;
        case "7":
            getDay(7);
            break;
        default:
            console.log("No such day exists!");
            break;
    }
    rl.close();
});
