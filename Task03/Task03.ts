//Write a simple program to display the day of the week based on the date/ (1 - 7) entered. Pls try to use Enum and Generics to implement the same

var readline = require('readline');

enum Days {
  Sunday = 1,
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday
}

function getDay(d: number): String {
  console.log(Days[d])
    return Days[d];
}
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  
  rl.question('Enter the sequence number for week days [1-7] --> ', (answer) => {
    switch(answer) {
        case "1":
        getDay(1);
        break;
      case "2":
        getDay(2);
        break;
      case "3":
        getDay(3);
        break;
      case "4":
        getDay(4);
        break;
      case "5":
        getDay(5);
        break;
      case "6":
        getDay(6);
        break;
      case "7":
        getDay(7);
        break;
      default:
        console.log("No such day exists!");
        break;
    }
    rl.close();
  });
